import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const bodyElement = document.createElement('div');
  bodyElement.textContent = fighter.name;
  // call showModal function
  showModal({
    title: 'Winner',
    bodyElement: bodyElement
  })
}
