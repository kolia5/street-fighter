import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if(fighter){
    const imageElement = createFighterImage(fighter);
    const fighterInfoElement = createFighterInfo(fighter);
    fighterElement.append(imageElement, fighterInfoElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo({ name, health, attack, defense }) {
  const fighterInfoElement = createElement({tagName: 'div', className: 'fighter-preview___info'});

  const nameElement = createElement({tagName: 'p', className: 'fighter-name'});
  nameElement.textContent = `Name: ${name}`;

  const healthElement = createElement({tagName: 'p', className: 'fighter-health'});
  healthElement.textContent = `Health: ${health}`;

  const attackElement = createElement({tagName: 'p', className: 'fighter-attack'});
  attackElement.textContent = `Attack: ${attack}`;

  const defenseElement = createElement({tagName: 'p', className: 'fighter-defense'})
  defenseElement.textContent = `Defense: ${defense}`;

  fighterInfoElement.append(nameElement, healthElement, attackElement, defenseElement);

  return fighterInfoElement;
}
