import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    window.addEventListener('keyup', handleClick);
    let hitIntervalFirstFighter = null;
    let hitIntervalSecondFighter = null;

    function handleClick(e) {
      switch (e.code) {
        case controls.PlayerOneAttack:
          hitIntervalFirstFighter = hit(firstFighter, secondFighter);
          return
        case controls.PlayerOneBlock:
          clearInterval(hitIntervalSecondFighter)
          return ;
        case controls.PlayerTwoAttack:
          hitIntervalSecondFighter = hit(secondFighter, firstFighter);
          return
        case controls.PlayerTwoBlock:
          clearInterval(hitIntervalFirstFighter)
          return ;
        default:
          return ;
      }
    }

    function hit(playerOne, playerTwo) {
      const timerId = setTimeout(() => {
        playerTwo.health = playerTwo.health - getDamage(playerOne, playerTwo);
        decreaseIndicator(playerTwo);
        if(playerTwo.health < 0) resolve(playerOne)
      }, 1500);
      return timerId;
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0){
    return 0;
  }
  return damage
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const attack = fighter.attack;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const defense = fighter.defense;
  return defense * dodgeChance;
}

function decreaseIndicator(fighter) {
  const spanElements = document.querySelectorAll('.arena___fighter-name');
  const currentSpan = Array.from(spanElements).find(item => item.textContent === fighter.name);
  const fighterIndicatorElem = currentSpan.nextSibling.children[0];
  fighterIndicatorElem.style.width = (fighterIndicatorElem.offsetWidth - 10) + 'px';
}








































////////
