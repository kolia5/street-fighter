import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';
  #endpointFighterId = '';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    this.#endpointFighterId = `details/fighter/${id}.json`;
    try {
      const apiResult = await callApi(this.#endpointFighterId);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
